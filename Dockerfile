# ---- Base Node ---- #
FROM ubuntu:16.04 AS base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libssl-dev git-core libboost-all-dev libdb4.8-dev libdb4.8++-dev libevent-dev libminiupnpc-dev pkg-config && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ---- #
FROM base as build
RUN git clone https://github.com/Sterlingcoin/Sterlingcoin-1.5.2.1-Full-Release-Linux-Source /opt/sterlingcoin
RUN cd /opt/sterlingcoin/src && \
	make -f makefile.unix && \
	strip sterlingcoind

# ---- Release ---- #
FROM ubuntu:16.04 as release
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
	apt-get install -y  libboost-all-dev libdb4.8 libdb4.8++ libminiupnpc-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN groupadd -r sterlingcoin && useradd -r -m -g sterlingcoin sterlingcoin
RUN mkdir /data
RUN chown sterlingcoin:sterlingcoin /data
COPY --from=build /opt/sterlingcoin/src/sterlingcoind /usr/local/bin/
USER sterlingcoin
VOLUME /data
EXPOSE 1141 8311
CMD ["/usr/local/bin/sterlingcoind", "-datadir=/data", "-conf=/data/sterlingcoin.conf", "-server", "-txindex", "-printtoconsole"]